# -*- coding: utf-8 -*-
"""
Module to get configuration from OpenShift environment vars.
"""

import os
from sqlalchemy import (
    create_engine,
    engine_from_config
)

def get_engine(settings):
    "Function to get a valid SQL Alchemy engine object"
    if settings["sqlalchemy.url"] == "openshift-environment":
        return create_engine(os.environ.get("OPENSHIFT_POSTGRESQL_DB_URL",
                                            "No Environment Variable Seted"))
    else:
        return engine_from_config(settings, 'sqlalchemy.')


def get_data_dir(settings):
    "Function to get data-dir path"
    if settings["data_dir"] == "openshift-environment":
        return os.environ.get("OPENSHIFT_DATA_DIR", 
                              "No Environment Variable Seted")
    else:
        return settings["data_dir"]


def get_temp_dir(settings):
    "Function to get temporary dir path"
    if settings["temporary_dir"] == "openshift-environment":
        return os.environ.get("OPENSHIFT_DATA_DIR", 
                              "No Environment Variable Seted")
    else:
        return settings["temporary_dir"]


