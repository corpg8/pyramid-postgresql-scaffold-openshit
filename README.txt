A Pyramid with PostgreSQL quickstart for OpenShift
==================================================

This is a Scaffold for Pyramid Web Frameworks apps over OpenShift

Getting Started
---------------

type in your terminal::

  rhc app create pyramidapp python-2.7 postgresql-9.2 --from-code=https://gitlab.com/G8Labs/Pyramid-PostgreSQL-Scaffold-OpenShift.git

after creating your app, move to your local repo, visit your app and initialize
the database schema::

  $ cd pyramidapp/
  $ rhc ssh
  r> initialize_PyramidApp_db app-root/repo/production.ini
  r> exit
  $

In order to use this repo locally you need to create a virtualenv inside your
repo directory, install your app on it and initialize a local database::

  $ virtualenv env
  $ env/bin/python setup.py develop

Enjoy!




--
Crafted with ❤️️ by G8Labs.

